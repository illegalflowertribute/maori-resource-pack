
package net.mcreator.maoriresourcepack.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.util.ResourceLocation;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.Item;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.mcreator.maoriresourcepack.ElementsMaoriResourcePack;

@ElementsMaoriResourcePack.ModElement.Tag
public class ItemWhaleboneToki extends ElementsMaoriResourcePack.ModElement {
	@GameRegistry.ObjectHolder("maoriresourcepack:whalebonetokihelmet")
	public static final Item helmet = null;
	@GameRegistry.ObjectHolder("maoriresourcepack:whalebonetokibody")
	public static final Item body = null;
	@GameRegistry.ObjectHolder("maoriresourcepack:whalebonetokilegs")
	public static final Item legs = null;
	@GameRegistry.ObjectHolder("maoriresourcepack:whalebonetokiboots")
	public static final Item boots = null;
	public ItemWhaleboneToki(ElementsMaoriResourcePack instance) {
		super(instance, 4);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("WHALEBONETOKI", "maoriresourcepack:diamond_", 20, new int[]{2, 5, 6, 2}, 9,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("item.armor.equip_generic")),
				1f);
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.CHEST).setUnlocalizedName("whalebonetokibody")
				.setRegistryName("whalebonetokibody").setCreativeTab(CreativeTabs.COMBAT));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(body, 0, new ModelResourceLocation("maoriresourcepack:whalebonetokibody", "inventory"));
	}
}
