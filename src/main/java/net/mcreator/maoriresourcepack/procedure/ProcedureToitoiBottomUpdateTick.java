package net.mcreator.maoriresourcepack.procedure;

import net.minecraft.world.World;
import net.minecraft.util.math.BlockPos;
import net.minecraft.block.state.IBlockState;
import net.minecraft.block.properties.IProperty;

import net.mcreator.maoriresourcepack.block.BlockToitoiTop;
import net.mcreator.maoriresourcepack.ElementsMaoriResourcePack;

import java.util.Map;

@ElementsMaoriResourcePack.ModElement.Tag
public class ProcedureToitoiBottomUpdateTick extends ElementsMaoriResourcePack.ModElement {
	public ProcedureToitoiBottomUpdateTick(ElementsMaoriResourcePack instance) {
		super(instance, 28);
	}

	public static void executeProcedure(java.util.HashMap<String, Object> dependencies) {
		if (dependencies.get("x") == null) {
			System.err.println("Failed to load dependency x for procedure ToitoiBottomUpdateTick!");
			return;
		}
		if (dependencies.get("y") == null) {
			System.err.println("Failed to load dependency y for procedure ToitoiBottomUpdateTick!");
			return;
		}
		if (dependencies.get("z") == null) {
			System.err.println("Failed to load dependency z for procedure ToitoiBottomUpdateTick!");
			return;
		}
		if (dependencies.get("world") == null) {
			System.err.println("Failed to load dependency world for procedure ToitoiBottomUpdateTick!");
			return;
		}
		int x = (int) dependencies.get("x");
		int y = (int) dependencies.get("y");
		int z = (int) dependencies.get("z");
		World world = (World) dependencies.get("world");
		if ((world.isAirBlock(new BlockPos((int) x, (int) (y - 1), (int) z)))) {
			world.setBlockToAir(new BlockPos((int) x, (int) y, (int) z));
		} else {
			{
				BlockPos _bp = new BlockPos((int) x, (int) (y + 1), (int) z);
				IBlockState _bs = BlockToitoiTop.block.getDefaultState();
				IBlockState _bso = world.getBlockState(_bp);
				for (Map.Entry<IProperty<?>, Comparable<?>> entry : _bso.getProperties().entrySet()) {
					IProperty _property = entry.getKey();
					if (_bs.getPropertyKeys().contains(_property))
						_bs = _bs.withProperty(_property, (Comparable) entry.getValue());
				}
				world.setBlockState(_bp, _bs, 3);
			}
		}
	}
}
