package net.mcreator.maoriresourcepack.procedure;

import net.minecraft.world.World;
import net.minecraft.util.math.BlockPos;
import net.minecraft.block.state.IBlockState;
import net.minecraft.block.material.Material;

import net.mcreator.maoriresourcepack.block.BlockCabbageTop;
import net.mcreator.maoriresourcepack.block.BlockCabbageMiddle;
import net.mcreator.maoriresourcepack.ElementsMaoriResourcePack;

@ElementsMaoriResourcePack.ModElement.Tag
public class ProcedureCabbageBottomUpdateTick extends ElementsMaoriResourcePack.ModElement {
	public ProcedureCabbageBottomUpdateTick(ElementsMaoriResourcePack instance) {
		super(instance, 19);
	}

	public static void executeProcedure(java.util.HashMap<String, Object> dependencies) {
		if (dependencies.get("x") == null) {
			System.err.println("Failed to load dependency x for procedure CabbageBottomUpdateTick!");
			return;
		}
		if (dependencies.get("y") == null) {
			System.err.println("Failed to load dependency y for procedure CabbageBottomUpdateTick!");
			return;
		}
		if (dependencies.get("z") == null) {
			System.err.println("Failed to load dependency z for procedure CabbageBottomUpdateTick!");
			return;
		}
		if (dependencies.get("world") == null) {
			System.err.println("Failed to load dependency world for procedure CabbageBottomUpdateTick!");
			return;
		}
		int x = (int) dependencies.get("x");
		int y = (int) dependencies.get("y");
		int z = (int) dependencies.get("z");
		World world = (World) dependencies.get("world");
		if ((!((world.getBlockState(new BlockPos((int) x, (int) (y + 1), (int) z))).getMaterial() == Material.PLANTS))) {
			{
				BlockPos _bp = new BlockPos((int) x, (int) (y + 1), (int) z);
				IBlockState _bs = BlockCabbageMiddle.block.getDefaultState();
				world.setBlockState(_bp, _bs, 3);
			}
			{
				BlockPos _bp = new BlockPos((int) x, (int) (y + 2), (int) z);
				IBlockState _bs = BlockCabbageTop.block.getDefaultState();
				world.setBlockState(_bp, _bs, 3);
			}
		}
		if ((world.isAirBlock(new BlockPos((int) x, (int) (y - 1), (int) z)))) {
			world.setBlockToAir(new BlockPos((int) x, (int) y, (int) z));
		}
	}
}
